package net.pl3x.kekera.minitennis;

import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public enum Sound {

	BALL_HIT("ballhit.wav"),
	GAMEOVER("gameover.wav"),
	BACKGROUND_LOOP("backgroundloop.wav"),
	TITLE_LOOP("title.wav");

	private URL sound;
	private Clip clip;

	Sound(String soundFile) {
		this.sound = Sound.class.getResource("/resources/" + soundFile);
	}

	private Clip newClip() {
		try {
			clip = AudioSystem.getClip();
			AudioInputStream stream = AudioSystem.getAudioInputStream(sound);
			clip.open(stream);

		} catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
			return null;
		}
		return clip;
	}

	public void loop() {
		clip = newClip();
		if (clip == null) {
			return;
		}
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	public void play() {
		clip = newClip();
		if (clip == null) {
			return;
		}
		clip.start();
	}

	public void stop() {
		if (clip == null) {
			return;
		}
		clip.stop();
		clip.setFramePosition(0);
	}

}
