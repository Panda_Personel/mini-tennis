package net.pl3x.kekera.minitennis;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Ball {
	private Game game;
	private int x = 0;
	private int y = 0;
	private int speedX = 1;
	private int speedY = 1;
	private static final int BALLSIZE = 30;

	public Ball(Game game) {
		this.game = game;
	}

	public void move() {

		boolean changeDirection = true;

		if (x + speedX * game.getSpeed() < 0) {
			speedX = 1;
		}

		else if (x + speedX * game.getSpeed() > game.getWidth() - BALLSIZE) {
			speedX = -1;
		}

		else if (y + speedY * game.getSpeed() < 0) {
			speedY = 1;
		}

		else if (y + speedY * game.getSpeed() > game.getHeight() - BALLSIZE) {
			game.gameOver();
		}

		else if (collision()) {
			speedY = -1;
			y = game.paddle.getTopY() - BALLSIZE;
			game.addScore();
			
			game.updateSpeed();
		}

		else {
			changeDirection = false;
		}

		if (changeDirection) {
			Sound.BALL_HIT.stop();
			Sound.BALL_HIT.play();
		}

		x = x + speedX * game.getSpeed();
		y = y + speedY * game.getSpeed();
	}

	private boolean collision() {
		return game.paddle.getBounds().intersects(getBounds());
	}

	public void paint(Graphics2D g) {
		g.fillOval(x, y, BALLSIZE, BALLSIZE);
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, BALLSIZE, BALLSIZE);
	}

}
