package net.pl3x.kekera.minitennis;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel {
	public Ball ball;
	public Paddle paddle;
	private int speed = 1;
	private int score = 0;

	public Game() {
		
		this.ball = new Ball(this);
		this.paddle = new Paddle(this);

		addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				paddle.keyPressed(e);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				paddle.keyReleased(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// ignore
			}

		});

		setFocusable(true);
		
		Sound.BACKGROUND_LOOP.loop();
	}

	public int getScore() {
		return score;
	}
	
	public void addScore() {
		score++;
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public void updateSpeed() {
		speed = (getScore() / 5) + 1;
	}

	public void gameOver() {
		
		Sound.BACKGROUND_LOOP.stop();
		Sound.GAMEOVER.play();
		
		JOptionPane.showMessageDialog(this, "Game Over", "GameOver", JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
	}

	public void move() {
		ball.move();
		paddle.move();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setColor(Color.BLACK);
		ball.paint(g2d);
		paddle.paint(g2d);
		
		g2d.setColor(Color.GRAY);
		g2d.setFont(new Font("Verdana", Font.BOLD, 30));
		g2d.drawString("Score: " + String.valueOf(getScore()),10,30);

	}
}
