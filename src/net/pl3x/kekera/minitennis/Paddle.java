package net.pl3x.kekera.minitennis;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

public class Paddle {
	private Game game;
	private int x;
	private int speedX;
	private static final int Y = 330;
	private static final int WIDTH = 60;
	private static final int HEIGHT = 10;

	public Paddle(Game game) {
		this.game = game;
	}

	public void move() {
		if (x + speedX * game.getSpeed() > 0 && x + speedX * game.getSpeed() < game.getWidth() - WIDTH) {
			x = x + speedX * game.getSpeed();
		}
	}

	public void paint(Graphics2D g) {
		g.fillRect(x, Y, WIDTH, HEIGHT);
	}

	public void keyReleased(KeyEvent e) {
		speedX = 0;
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			speedX = -1;
		}

		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			speedX = 1;
		}
	}

	public Rectangle getBounds() {
		return new Rectangle(x, Y, WIDTH, HEIGHT);
	}

	public int getTopY() {
		return Y;
	}

}
